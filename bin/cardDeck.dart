import 'dart:io';

class cardDeck {
  String card1() {
    return "┌────────┐\n" +
        "│░░███╗░░│\n" +
        "│░████║░░│\n" +
        "│██╔██║░░│\n" +
        "│╚═╝██║░░│\n" +
        "│███████╗│\n" +
        "│╚══════╝│\n" +
        "└────────┘\n" +
        "The Magician : You will have outstanding career luck because of your abilities.\n" +
        "Effect Card  : + 5 Point ";
  }

  String card2() {
    return "┌────────┐\n" +
        "│██████╗░│\n" +
        "│╚════██╗│\n" +
        "│░░███╔═╝│\n" +
        "│██╔══╝░░│\n" +
        "│███████╗│\n" +
        "│╚══════╝│\n" +
        "└────────┘\n" +
        "The High : Priestess You will have outstanding career luck because of your abilities.\n" +
        "Effect Card : + 3 Point ";
  }

  String card3() {
    return "┌────────┐\n" +
        "│██████╗░│\n" +
        "│╚════██╗│\n" +
        "│░█████╔╝│\n" +
        "│░╚═══██╗│\n" +
        "│██████╔╝│\n" +
        "│╚═════╝░│\n" +
        "└────────┘\n" +
        "The Empress : Your luck will come from the patron female boss. \n" +
        "Effect Card : - 2 Point ";
  }

  String card4() {
    return "┌────────┐\n" +
        "│░░██╗██╗│\n" +
        "│░██╔╝██║│\n" +
        "│██╔╝░██║│\n" +
        "│███████║│\n" +
        "│╚════██║│\n" +
        "│░░░░░╚═╝│\n" +
        "└────────┘\n" +
        "The Emperor : You still don't have much luck, but get a little luck from a respected adult. \n" +
        "Effect Card  : - 4 Point ";
  }

  String card5() {
    return "┌────────┐\n" +
        "│███████╗│\n" +
        "│██╔════╝│\n" +
        "│██████╗░│\n" +
        "│╚════██╗│\n" +
        "│██████╔╝│\n" +
        "│╚═════╝░│\n" +
        "└────────┘\n" +
        "The Hierophant : You don't have luck \n" +
        "Effect Card : - 3 Point ";
  }

  String card6() {
    return "┌────────┐\n" +
        "│░█████╗░│\n" +
        "│██╔═══╝░│\n" +
        "│██████╗░│\n" +
        "│██╔══██╗│\n" +
        "│╚█████╔╝│\n" +
        "│░╚════╝░│\n" +
        "└────────┘\n" +
        "The Lover : Lack of luck during this time depends on your own abilities. \n" +
        "Effect Card  : 0 Point ";
  }

  String card7() {
    return "┌────────┐\n" +
        "│███████╗│\n" +
        "│╚════██║│\n" +
        "│░░░░██╔╝│\n" +
        "│░░░██╔╝░│\n" +
        "│░░██╔╝░░│\n" +
        "│░░╚═╝░░░│\n" +
        "└────────┘\n" +
        "The Chariot : There may be a small fortune from your friends during this time. or from your own lover \n" +
        "Effect Card  : + 1 Point ";
  }

  String card8() {
    return "┌────────┐\n" +
        "│░█████╗░│\n" +
        "│██╔══██╗│\n" +
        "│╚█████╔╝│\n" +
        "│██╔══██╗│\n" +
        "│╚█████╔╝│\n" +
        "│░╚════╝░│\n" +
        "└────────┘\n" +
        "Strength : There is no luck in any aspect, but you have a little luck in the gamble.\n" +
        "Effect Card : - 2 Point ";
  }

  String card9() {
    return "┌────────┐\n" +
        "│░█████╗░│\n" +
        "│██╔══██╗│\n" +
        "│╚██████║│\n" +
        "│░╚═══██║│\n" +
        "│░█████╔╝│\n" +
        "│░╚════╝░│\n" +
        "└────────┘\n" +
        "The hermit : Your luck is no more charity, life is very lucky.\n" +
        "Effect Card : - 10 Point ";
  }

  String card10() {
    return "┌────────────────┐\n" +
        "│░░███╗░░░█████╗░│\n" +
        "│░████║░░██╔══██╗│\n" +
        "│██╔██║░░██║░░██║│\n" +
        "│╚═╝██║░░██║░░██║│\n" +
        "│███████╗╚█████╔╝│\n" +
        "│╚══════╝░╚════╝░│\n" +
        "└────────────────┘\n" +
        "Wheel of Fortune : You have great luck in financial affairs, you may get a promotion or earn extra money.\n" +
        "Effect Card : + 2 Point ";
  }

  String card11() {
    return "┌────────────────┐\n" +
        "│░░███╗░░░░███╗░░│\n" +
        "│░████║░░░████║░░│\n" +
        "│██╔██║░░██╔██║░░│\n" +
        "│╚═╝██║░░╚═╝██║░░│\n" +
        "│███████╗███████╗│\n" +
        "│╚══════╝╚══════╝│\n" +
        "└────────────────┘\n" +
        "Justice : I haven't had much luck in this period. But if there is, it may be a small souvenir.\n" +
        "Effect Card : + 1 Point ";
  }

  String card12() {
    return "┌────────────────┐\n" +
        "│░░███╗░░██████╗░│\n" +
        "│░████║░░╚════██╗│\n" +
        "│██╔██║░░░░███╔═╝│\n" +
        "│╚═╝██║░░██╔══╝░░│\n" +
        "│███████╗███████╗│\n" +
        "│╚══════╝╚══════╝│\n" +
        "└────────────────┘\n" +
        "The hanged man : Your face has no luck getting in like this.\n" +
        "Effect Card : - 5 Point ";
  }

  String card13() {
    return "┌────────────────┐\n" +
        "│░░███╗░░██████╗░│\n" +
        "│░████║░░╚════██╗│\n" +
        "│██╔██║░░░█████╔╝│\n" +
        "│╚═╝██║░░░╚═══██╗│\n" +
        "│███████╗██████╔╝│\n" +
        "│╚══════╝╚═════╝░│\n" +
        "└────────────────┘\n" +
        "Death : During this time of luck, you will lose severe money or lost items, be careful and careful.\n" +
        "Effect Card : - 6 Point ";
  }

  String card14() {
    return "┌────────────────┐\n" +
        "│░░███╗░░░░██╗██╗│\n" +
        "│░████║░░░██╔╝██║│\n" +
        "│██╔██║░░██╔╝░██║│\n" +
        "│╚═╝██║░░███████║│\n" +
        "│███████╗╚════██║│\n" +
        "│╚══════╝░░░░░╚═╝│\n" +
        "Temperance : No luck, but not bad, if you make a lot of merits, everything will be fine.\n" +
        "Effect Card : - 7 Point ";
  }

  String card15() {
    return "┌────────────────┐\n" +
        "│░░███╗░░███████╗│\n" +
        "│░████║░░██╔════╝│\n" +
        "│██╔██║░░██████╗░│\n" +
        "│╚═╝██║░░╚════██╗│\n" +
        "│███████╗██████╔╝│\n" +
        "│╚══════╝╚═════╝░│\n" +
        "└────────────────┘\n" +
        "The Devil : There is no luck during this period, but there will be little luck in gambling. \n" +
        "Effect Card : - 3 Point ";
  }

  String card16() {
    return "┌────────────────┐\n" +
        "│░░███╗░░░█████╗░│\n" +
        "│░████║░░██╔═══╝░│\n" +
        "│██╔██║░░██████╗░│\n" +
        "│╚═╝██║░░██╔══██╗│\n" +
        "│███████╗╚█████╔╝│\n" +
        "│╚══════╝░╚════╝░│\n" +
        "└────────────────┘\n" +
        "The tower : No luck came in at all. In addition, be careful of misfortune. \n" +
        "Effect Card : - 2 Point ";
  }

  String card17() {
    return "┌────────────────┐\n" +
        "│░░███╗░░███████╗│\n" +
        "│░████║░░╚════██║│\n" +
        "│██╔██║░░░░░░██╔╝│\n" +
        "│╚═╝██║░░░░░██╔╝░│\n" +
        "│███████╗░░██╔╝░░│\n" +
        "│╚══════╝░░╚═╝░░░│\n" +
        "└────────────────┘\n" +
        "THE STAR : It's a fortune about work.\n" +
        "Effect Card : + 1 Point ";
  }

  String card18() {
    return "┌────────────────┐\n" +
        "│░░███╗░░░█████╗░│\n" +
        "│░████║░░██╔══██╗│\n" +
        "│██╔██║░░╚█████╔╝│\n" +
        "│╚═╝██║░░██╔══██╗│\n" +
        "│███████╗╚█████╔╝│\n" +
        "│╚══════╝░╚════╝░│\n" +
        "└────────────────┘\n" +
        "The Moon : Don't take any risks because you're not very lucky right now. \n" +
        "Effect Card : - 2 Point ";
  }

  String card19() {
    return "┌────────────────┐\n" +
        "│░░███╗░░░█████╗░│\n" +
        "│░████║░░██╔══██╗│\n" +
        "│██╔██║░░╚██████║│\n" +
        "│╚═╝██║░░░╚═══██║│\n" +
        "│███████╗░█████╔╝│\n" +
        "│╚══════╝░╚════╝░│\n" +
        "└────────────────┘\n" +
        "The Sun : There will be a fortune in love may be a boyfriend or someone who secretly bakes as a girlfriend.\n" +
        "Effect Card : + 15 Point ";
  }

  String card20() {
    return "┌────────────────┐\n" +
        "│██████╗░░█████╗░│\n" +
        "│╚════██╗██╔══██╗│\n" +
        "│░░███╔═╝██║░░██║│\n" +
        "│██╔══╝░░██║░░██║│\n" +
        "│███████╗╚█████╔╝│\n" +
        "│╚══════╝░╚════╝░│\n" +
        "└────────────────┘\n" +
        "JUDGEMENT : You will have an unexpected fortune coming in. \n" +
        "Effect Card : + 7 Point ";
  }

  String card21() {
    return "┌────────────────┐\n" +
        "│██████╗░░░███╗░░│\n" +
        "│╚════██╗░████║░░│\n" +
        "│░░███╔═╝██╔██║░░│\n" +
        "│██╔══╝░░╚═╝██║░░│\n" +
        "│███████╗███████╗│\n" +
        "│╚══════╝╚══════╝│\n" +
        "└────────────────┘\n" +
        "The World : During this time you are full of luck, you can do anything tomorrow. \n" +
        "Effect Card : + 5 Point ";
  }

  String card22() {
    return "┌────────────────┐\n" +
        "│██████╗░██████╗░│\n" +
        "│╚════██╗╚════██╗│\n" +
        "│░░███╔═╝░░███╔═╝│\n" +
        "│██╔══╝░░██╔══╝░░│\n" +
        "│███████╗███████╗│\n" +
        "│╚══════╝╚══════╝│\n" +
        "└────────────────┘\n" +
        "The Fool : Your luck is but doesn't trust too much \n" +
        "Effect Card : - 6  Point ";
  }
}
