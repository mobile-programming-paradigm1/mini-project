abstract class Game {
  var name = "";

  Game(this.name);
  void printStartGame();
  void printEndGame();
  void printRules();
}
